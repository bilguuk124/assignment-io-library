section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
        .loop:
            cmp byte[rdi+rax], 0 ; Проверить является ли текущий символ нуль-термирование
            je .end
            inc rax
            jmp .loop
        .end:   
            ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
        mov rsi, rdi        ; Указать на адрес строки, которого мы выводим
        call string_length  ; Узнать длину строки
        mov rdx, rax        ; Указать на длину строки
        mov rdi, 1          ; Указать на stdout
        mov rax, 1          ; 'write' syscall number
        syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
        push rdi            
        mov rsi, rsp        ; Указать на адрес символа вывода
        pop rdi             
        mov rax, 1          ; 'write' syscall number
        mov rdx, 1          ; Указать длину строки вывода (1)
        mov rdi, 1          ; Указать на stdout
        syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA        ; Указать на символ перевода строки 
    push rcx            ; Защищать регистр rcx
    call print_char     ; Вывести символ 0xA
    pop rcx
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
        xor rdx, rdx
        mov r8, 1             ; счётчик 
        mov r9, 10            ; делимое, использованное в функции
        mov rax, rdi          ; Взять аргумента в регистр аккумулятора
        dec rsp               
        mov byte [rsp], 0     ; Поставить конец строки в стеке
        .loop:                  
            div r9            ; Делить число на 10, чтобы взять последнее число
            mov rsi, rdx      ; Остаток деление - последнее число. Его записать в rsi
            add rsi, 48       ; Перевод в ASCII
            inc r8            ; инкремент счётчика
            dec rsp           ; Выделить байт для число
            mov [rsp], sil    ; Записать остаток деление в стеке
            xor rdx, rdx      ; Очистить регистр данных
            cmp rax, 0        ; Проверить последнее ли число
            jne .loop         ; Если нет то продолжаем цикл
        mov rdi, rsp          ; Записать результат в rdi, чтобы передать в следующую функцию
        push rcx              
        push rsi
        call print_string     ; Выводим результат
        pop rsi
        pop rcx
        add rsp, r8           ; Вернуть стек в оргинальное состояние
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
        xor rdx, rdx
        mov rax, rdi
        cmp rax, 0          ; Если число > 0 то сразу выводим
        jge .print          ; Если нет то выводим с минусом положительное число
        push rax
        push rdx
        push rcx
        mov rdi, '-'
        call print_char
        pop rcx
        pop rdx
        pop rax
        neg rax
        .print:
            mov rdi, rax
            push rcx
            call print_uint
            pop rcx
            ret
    

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax, 1
        mov r8, rdi                 ; Указатель на первую строку
        mov r9, rsi                 ; Указавель на вторую строку
        xor rcx, rcx
        xor rdi, rdi
        xor rsi, rsi
        .loop:
            mov dil, [r8+rcx]
            mov sil, [r9+rcx]
            cmp sil, dil
            jne .NotEqual
            cmp byte dil, 0         ; Если оба символа равен нулю мы остоновим
            je .end
            inc rcx
            jmp .loop
        .NotEqual:
            mov rax, 0
        .end:
            ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
        mov rdi, 0          ; Указать на stdin
        mov rdx, 1          ; Указать на длину строки ввода
        mov rax, 0          ; 'read' syscall
        dec rsp
        mov rsi, rsp
        syscall
        cmp rax, 0
        je .zero
        mov rax, [rsp]
        inc rsp
    ret 
        .zero:
            mov rax, 0
            inc rsp
            ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi
    mov r9, rsi

    .skip_whitespaces:          ; Цикл удаление предыдущих пробелов
        call read_char
        cmp al, 0x20
        je .skip_whitespaces
        cmp al, 0x9
        je .skip_whitespaces
        cmp al, 0xA
    xor rdx, rdx                ;counter
    jmp .write
    .loop:                      ; Цикл ввода символа
        push rdx
        call read_char
        pop rdx
    .write:                     ; Цикл проверки конец строки, если нет 
        cmp al, 0xA             ; то вызываем цикл ввода символа 
        je .end
        cmp al, 0x20
        je .end
        cmp al, 4
        je .end 
        cmp al, 0x9
        je .end
        cmp al, 0
        je .end
        inc rdx
        cmp rdx, r9
        jge .overflow
        dec rdx
        mov [r8+rdx], al
        inc rdx
        jmp .loop
    .end:                       ; Сохраняем результат в rax если всё нормально
        mov byte[r8+rdx],0
        mov rax, r8
        ret
    .overflow:                  ; Если слово слишком большой для буфера то прекращаем работу
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor r8, r8
    xor r9, r9
    mov r10, 10
    .loop: 
        mov r8b, [rdi + r9]
        cmp r8b, '0'
        jl .end
        cmp r8b, '9'
        jg .end                     ; Проверить число ли аргумент
        sub r8b, '0'                ; Перевод из ASCII в число
        mul r10                     ; Умножаем на основу системы счисления
        add rax, r8                 ; добавляем цифру
        inc r9                      ; инкрементируем длину числа
        jmp .loop
    .end:
        mov rdx, r9
        ret
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'      ; Проверяем отрицательное ли число
    jne parse_uint          ; Если нет то просто идем к фукцию parse_uint
    inc rdi                 
    call parse_uint         ; Если отрицателен то парсим положительное число
    neg rax                 ; и сделаем его отрицательным
    cmp rdx, 0              
    je .end
    inc rdx                 ; Добавляем минус в длину строки
    .end:
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    xor r9, r9
    push rdi
    call string_length              ; Узнаем длину строки
    pop rdi
    cmp rax, rdx                    ; Если длина строки меньше чем длины буфера
    jle .loop                       ; то идем в цикл
    mov rax, 0                      ; если больше то просто остановим работу
    ret
    .loop:                          
        mov r9b, [rdi+rcx]
        mov [rsi +rcx],r9b
        inc rcx
        cmp byte[rsi + rcx],0
        jne .loop
    mov rax, rdx
    ret
